<!-- HexPawn is created by Lee Jones (Moosebiter) under a GNU v2 license -->

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Login - HexPawn</title>
        <script src="jquery-1.11.1.min.js"></script>
        <link rel="stylesheet" type="text/css" href="style.css" />
    </head>
    <body>
        <div id="wrapper">
            <h2 class="center">HexPawn System</h2>
            <div class="container">
                <div class="buttonBar left panel">
                <legend>Login</legend>
                    <table>
                        <tr>
                            <td>User Name: </td><td><input type="text" id="username" placeholder=" User Name or Email" /></td>
                        </tr><tr>
                            <td>Password: </td><td><input type="password" id="password" placeholder=" Password" /></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <h6 class="center">HexPawn is created by Lee Jones (Moosebiter) under a GNU v2 license</h6>
    </body>
</html>
