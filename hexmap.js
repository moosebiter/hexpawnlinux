/*HexPawn is created by Lee Jones (Moosebiter) under a GNU v2 license*/

// Purpose: prints a hex grid into a label called "hex"
function DrawHexGrid(height, colNum, rowNum)
{
	if (!height)
	{
		height = document.getElementById("height").value;		// height of a hexagon from field
		document.getElementById("mapSelect").selectedIndex = "0";
	}
	if (!colNum)
		colNum = document.getElementById("columnNum").value;	// number of columns from number field
	if (!rowNum)
		rowNum = document.getElementById("rowNum").value;		// number of rows from number field

	var width = (Math.sqrt(3)/2) * (height);					// width of a hexagon
	var origin = [width/2 + width/4, height/2 + height/4];		// cartesian location of first hexagon's center (with margin 1/4 hex margin)
	var output = ""; 											// output string
	var centerX = origin[0];									// initialize the centerX of next hex
	var centerY = origin[1];									// initialize the centerY of next hex
	var svgHeight = ((3/4)*height*rowNum + height/4) + height/2;// height of svg area with margin
	var svgWidth = (width * colNum + (width/2)) + width/2;		// width of svg area with margin
	var mapName = document.getElementById("mapSelect");
		
	$(document).ready(function(){
		$.post("ColorMap.php", {mapName : mapName.value}, function(data, status){
			//alert(data);
			var dataArray = data.split("|");
			var index = 0;

			output += "<svg id='svgCanvas' height = " + svgHeight + " width = " + svgWidth + " xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'>";	// start svg
			output += "<g id='viewport'> ";
			for (var y=0; y<rowNum; y++)
			{
				for (var x=0; x<colNum; x++)
				{
					if (y % 2 == 0 && x == 0)					// if even row and first hex, set orign
						centerX = origin[0];
					else if (x == 0)							// if odd row and first hex, set origin
						centerX = origin[0] + (1/2) * width;				
				
					center = [centerX, centerY];				// set center
					output += DrawHex(height, center, x, y, dataArray[index]);	// draw hexagon
					index++;

					centerX += width;	
				}
				centerY += height*(3/4);
			}
			output += "\n</g>";									// end viewport group
			output += "\n</svg>";								// end svg
			document.getElementById("hex").innerHTML = output;	// output svg into label "hex"
			
			if (CoordsOn == false)								// set coordinates to current toggle
				$(".svgText").hide();
			
			document.getElementById("hexOptions").innerHTML = "<h3 align='center'>Select a hex to see its options.</h3>";
		});
	});		
}

// Purpose: draws a hex at of the given height at a given center point.
function DrawHex(height, center, x, y, terrainType)
{	
	var size = height/2;												// size of an edge of the hex (or a triangle)
	var myPoint;														// holds current vertex
	var points = "";													// initialize string of the verticies to empty string

	for (var i = 0; i < 6; i++)											// get each vertex
	{
		myPoint = AddPoint(center, size, (Math.PI / 3) * (i + 0.5));	// get next vertex in hexagon
				
		points += myPoint[0] + ", " + myPoint[1];						// put x and y coordinate of vertex into the string
		if (i < 5)
			points += ", ";												// add comma if not last pair
	}
	
	// set output string for polygon
	var output = "<polygon class='hex " + terrainType + "' id='" + x + ", " + y + "' points='" + points + "' onclick='HexSelectAndOptions(this);' />";	
	output += "<text class='svgText' text-anchor='middle' x='" + center[0] + "' y='" + (center[1]-height*(1/5)) + "' fill='black' font-size='" + height*(1/4) + "px' style='pointer-events:none;' >" + x + ", " + y + "</text>";
	
	if(terrainType)
	{
		var svgWidth = height*(1/2.4);
		var svgHeight = height*(1/2.4);
		var svgX = center[0] - svgWidth/2;
		var svgY = center[1] - svgHeight/2.5;

		output += "<image xlink:href='icons/" + terrainType + ".svg' x='" + svgX + "' y='" + svgY + "' width='" + svgWidth + "' height='" + svgHeight + "' style='pointer-events:none;' />";
	}

	return output;														// return output string
}

// Purpose: helper function for DrawHex() that will set the next point's x,y coordinate in a hexagon
function AddPoint(center, size, angle)
{
	var myX = center[0] + size * Math.cos(angle);						// get the x coordinate by moving size from center at angle
	var myY = center[1] + size * Math.sin(angle);						// get the y coordinate by moving size from center at angle

	return [myX, myY];													// return x and y coordinates
}

//________________________________________________________
var prevSelHex; //previously selected hex gloabal variable
var middleMouse;
//________________________________________________________

// Purpose: display the options by the hex grid for a selected hex
function HexSelectAndOptions(myObject){
	$('#hexGrid').click(function(e){
 		if(e.which != 2)
 		{ 	
 			var output;
			
			if (prevSelHex)
			{
				prevSelHex.classList.remove('selectedHex');	// remove class for styling (remove hex)
			}

			if (prevSelHex == myObject)
			{
				output="<h3 align='center'>Select a hex to see its options.</h3>";
				prevSelHex = false;
			}
			else
			{
				prevSelHex = myObject;
				myObject.classList.add('selectedHex');		// add class for styling (select hex)
				output = "<h3 align='center'>Information for slected hex: (" + myObject.id + ")</h3><hr>";


				// query database for this info:
				var myID = (myObject.id).split(", ");		// get id into array
				var x = myID[0];							// x coordinate of hex
				var y = myID[1];							// y coordinate of hex
				var mapName;

				if(document.getElementById("mapSelect").value != "default")
				{
					mapName = document.getElementById("mapSelect").value;

					$(document).ready(function(){
						$.post("GetTerrain.php", {mapName : mapName, x : x, y : y}, function(data, status){
							//alert(data);
							dataArray = data.split("|");
							output += "<table>";
							output += "<tr><td>Hex Alias: </td><td>" + dataArray[0] + "</td></tr>";
							output += "<tr><td>Terrain Type: </td><td>" + dataArray[1] + "</td></tr>";
							output += "<tr><td colspan='2'>" + dataArray[2] + "</td></tr>";
							output += "<tr><td>Elements: </td><td>" + dataArray[3] + "</td></tr>";
							output += "<tr><td colspan='2'>" + dataArray[4] + "</td></tr></table>";
							
							document.getElementById("hexOptions").innerHTML = output;
						});
					});		
				}
				else
					output += "No map currently loaded.";
			}
			document.getElementById("hexOptions").innerHTML = output;
		}
	});
}

function RescaleGrid()
{
	var createHeight = document.getElementById('height').value;
	var displayHeight = document.getElementById('displayheight').value; 
	createHeight = displayHeight;
	DrawHexGrid(displayHeight);
}

// Purpose: to generate a map's features and save them to the database: in progress...
function GenerateMap(colNum, rowNum){
	var mapName = document.getElementById("mapName").value;	// get the map name
	var numTerrainTypes;
	var numElementTypes;
	var terrainTypes = [];							// hold the terrain type for all hexes
	var elements = [];								// hold the elements for all hexes
	var dataArray = [];

	if (!mapName)
	{
		alert("Please enter a map name.");
		return;
	}

	if (!colNum)
		colNum = document.getElementById("columnNum").value;
	if (!rowNum)
		rowNum = document.getElementById("rowNum").value; 
	
	$(document).ready(function(){
		$.post("GenerateMap.php", function(data, status){
			dataArray = data.split("|");
			dataArray.reverse();
		
			numElementTypes = dataArray.pop();
			numTerrainTypes = dataArray.pop();

			// create the terrain and element arrays
			var token = dataArray.pop();
			while (token != "ENDELEMENTS")
			{
				elements.push(token);
				token = dataArray.pop();
			}
			token = dataArray.pop();
			while (token)
			{
				terrainTypes.push(token);
				token = dataArray.pop();
			}
						
			$(document).ready(function(){
				$.post("InsertTerrain.php", {terrainTypes : terrainTypes, elements : elements, mapName : mapName, width : colNum, height : rowNum}, function(data){
					//alert("data: " + data);
					alert("You have created a new map, access it with the Display Map pane.");
					location.reload();
				});
			});
			
		});
	});
}
		

function SelectMap()
{
	var mapName = document.getElementById("mapSelect").value;
	
	// get width and height and reprint grid
	$(document).ready(function(){
		$.post("GetMap.php", {mapName : mapName}, function(data, status){
			dataArray = data.split("|");
			var scale = document.getElementById("displayheight").value;
			
			DrawHexGrid(scale, dataArray[0], dataArray[1]);
			document.getElementById("columnNum").value = dataArray[0];
			document.getElementById("rowNum").value = dataArray[1];
		});
	});
}

function SetTransformAttributes(scale, x, y){
	var transformString;

	transformString = "translate(" + x + ", " + y + ") ";
	transformString += "scale(" + scale + ") ";
	transformString += "translate(-" + x + ", -" + y + ")";

	return transformString;
}

// current slider only handles whole numbers... gross.
function ZoomSlider(zoomLevel)
{
	var svgCanvas = document.getElementById("svgCanvas");
	var viewport = document.getElementById("viewport");
	var canvasWidth = svgCanvas.getAttribute("width");
	var canvasHeiight = svgCanvas.getAttribute("height");
	var transformAttributes;
	
	scale = zoomLevel.value * 0.1;
	
	transformAttributes = SetTransformAttributes(scale, canvasWidth / 2, canvasHeiight / 2)
	
	viewport.setAttribute('transform', transformAttributes);					
}

function MouseZoom(direction, x, y)
{
	var svgCanvas = document.getElementById("svgCanvas");
	var viewport = document.getElementById("viewport");
	var transformAttributes;

	if(direction == 1 && scale < 5)
		scale += 0.1;
	else if(direction == 0 && scale >1)
		scale -= 0.1;
	else 
		return;

	transformAttributes = SetTransformAttributes(scale, x, y);
	
	document.getElementById("zoom").value = scale*10;

	viewport.setAttribute('transform', transformAttributes);					
}

function Pan(x, y){
	var viewport = document.getElementById('viewport');
	var transformAttributes;

	transformAttributes = SetTransformAttributes(scale, x, y);
	viewport.setAttribute('transform', transformAttributes);
}

// JQUERY EVENT LISTENERS:________________________________________________________
//________________________________________________________
var CoordsOn = false;	// declaration of global variable for toggling coordinates
var scale = 1;
var middleMouseIsUp = true;
//________________________________________________________

// toggle the coordinates
$(document).ready(function(){
	$(".svgText").hide();
	$("#toggleCoords").click(function(){
		if (CoordsOn == true){
			$(".svgText").hide();
			CoordsOn = false;
		}
		else{
			$(".svgText").show();
			CoordsOn = true;
		}
	});
});

//register mouse scrolling up and down on the hex grid. 
//Implement zoom here if you can get the coords from the mouse position.
$(document).ready(function(){
    $('#hexGrid').bind('mousewheel', function(e){
    	var x
    	var y;
    	var parentOffset = $(this).parent().offset();
    	x = e.pageX - parentOffset.left;
        y = e.pageY - parentOffset.top;
        e.preventDefault();	//prevent page-scrolling

        if(e.originalEvent.wheelDelta > 0)
            MouseZoom(1, x, y);
        else
            MouseZoom(0, x, y);

        return true;	//revert page-scrolling
    });
});

// get a middle mouse down event (for panning maybe)
$(document).ready(function(){
	$('#hexGrid').mousedown(function(e){
		e.preventDefault(); // prevent page-panning while panning
		if (e.which == 2){ // if middleclick
			middleMouseIsUp = false;
			$('#hexGrid').mousemove(function(f){
				if(!middleMouseIsUp){
					var x;
					var y;
					var parentOffset = $(this).parent().offset();
					x = f.pageX - parentOffset.left;
	            	y = f.pageY - parentOffset.top;
					Pan(x,y);
				}
			});
		return true;	// revert page-panning
		}
	});
});

$(document).ready(function(){
	$('#hexGrid').mouseup(function(e){
		middleMouseIsUp = true;
	});
});
