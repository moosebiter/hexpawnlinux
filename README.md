Hex-Pawn
========
License
-----------
HexPawn is published under a GNU v2 liscence. See "LISCENCE" in this Git Repository for more information or go to http://www.gnu.org/licenses/gpl-2.0.html

Description
-----------
HexPawn is a web based application for generating hexagon based maps with terrain and features for use in hexagon based games. The intention is to make it a framework for many different themes of games and to eventually add in mechanics for actually playing games directly through the web browser. Additionally, it is intended to serve as an aid for sandbox style role playing game campaigns. It would be nice to integrate rules from OGL games as well such as Paizo's Pathfinder.

Installation
-----------
The files included in this repository do not currently include the webConfig.php file which is necessary to connect to a database. You will need to create your own database and file unless I give permission to access my database and provide it to you.

Credits
-----------
<h4>Elizabeth Jones</h4>
Elizabeth Jones created the icons for terrain types and are currently used with her permission. Use of these icons outside of the scope of HexPawn requires her permission.
elizabethorrjones@gmail.com

<h4>Paizo</h4>
Currently the method for generating terrain is from Paizo's Pathfinder Exploration system which can be found here: 
[PRD/UltimateCampagin/Exploration](http://paizo.com/pathfinderRPG/prd/ultimateCampaign/campaignSystems/exploration.html). 

Pathfinder Roleplaying Game Ultimate Campaign. © 2013, Paizo Publishing, LLC; Authors: Jesse Benner, Benjamin Bruck, Jason Bulmahn, Ryan Costello, Adam Daigle, Matt Goetz, Tim Hitchcock, James Jacobs, Ryan Macklin, Colin McComb, Jason Nelson, Richard Pett, Stephen Radney-MacFarland, Patrick Renie, Sean K Reynolds, F. Wesley Schneider, James L. Sutter, Russ Taylor, and Stephen Townshend.

The intention is to change this to create a biome based terrain and feature generation system that is specific to HexPawn.

