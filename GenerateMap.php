<?php 
/*HexPawn is created by Lee Jones (Moosebiter) under a GNU v2 license*/

	include("webConfig.php");

	$query = "SELECT DISTINCT Element FROM ELEMENTS";	// get the count of the different element types
	$result = mysqli_query($con, $query);
	$numElements = mysqli_num_rows($result);

	$query = "SELECT Type FROM TERRAIN_TYPES";			// get the count of different terraint types
	$result = mysqli_query($con, $query);
	$numTerrainTypes = mysqli_num_rows($result);

	$query = "SELECT DISTINCT Element FROM ELEMENTS";			// get all the elements
	$elements = mysqli_query($con, $query);

	$query = "SELECT Type FROM TERRAIN_TYPES";					// get all the terrain types
	$types = mysqli_query($con, $query);

	echo $numElements . "|" . $numTerrainTypes;

	while ($row = mysqli_fetch_array($elements))
		echo "|" . $row['Element'];
	echo "|ENDELEMENTS";
	while ($row = mysqli_fetch_array($types))
		echo "|" . $row['Type'];
	
	mysqli_close($con);
?>