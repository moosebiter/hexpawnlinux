<!-- HexPawn is created by Lee Jones (Moosebiter) under a GNU v2 license -->

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Home - HexPawn</title>
        <script src="jquery-1.11.1.min.js"></script>
        <link rel="stylesheet" type="text/css" href="style.css" />
    </head>
    <body>
        <div id="wrapper">
            <div class="container">
                <h2 class="center">HexPawn Home</h2>
                <div class="column">
                <div class="buttonBar left panel">
                    <legend>My Games</legend>
                        +New Game<br>
                        <hr>
                        game1<br>
                        game2<br>
                    </div>
                </div>
                <div class="column">
                    <div class="buttonBar left panel">
                    <legend>My Friends</legend>
                        +Add Friend<br>
                        <hr>
                        friend1<br>
                        friend2<br>
                        <hr>
                        Requests:<br>
                        request1<br>
                        request2<br>
                    </div>
                </div>
            </div>
        </div>
        <h6 class="center">HexPawn is created by Lee Jones (Moosebiter) under a GNU v2 license</h6>
    </body>
</html>
