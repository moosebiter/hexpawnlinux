<?php
	/*HexPawn is created by Lee Jones (Moosebiter) under a GNU v2 license*/

	/*
		THIS FILE IS FOR A WAMP INSTALL USING NO MYSQL ROOT PASSWORD AND 
		CREATING THE DATABASE USING THE Creations.sql FILE.
		YOU WILL NEED TO ALTER THE CONNECTION STRING IF YOU ARE USING YOUR OWN DATABASE!
	*/

	// create the connection to the database.
	$con = mysqli_connect("localhost", "root", "", "HEXGAME");

?>